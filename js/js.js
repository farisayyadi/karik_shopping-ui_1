$(document).ready(function () {
    var isIOS = ((/iphone|ipad/gi).test(navigator.appVersion));
    var myDown = isIOS ? "touchstart" : "click";
    var lanDirection = 'right';
    var homePage = window.location.href.match(/.*\/(.*)$/)[1];

    lanDirection = homePage.match('en') ? "left" : "right";
  
    var options = {};
   
   
    var disable_click = false;

    $('body').on(myDown, '.menu i', function (e) {

        if (disable_click == false) {

            if ($(this).hasClass('animate')) {

                options[lanDirection] = -$(".dropdown").width() - 100 ;
                $('.menu-place').animate(options, 1000, function () {
               

                disable_click = false;
                });

                $(this).removeClass("animate");
            }
            else {

                $(this).addClass("animate");
                options[lanDirection] = 0;
                $('.menu-place').animate(options, 1000, function () {
                    disable_click = false;

                });

            }

            disable_click = true;
        }

    });


    $('body').on(myDown, '#btnform', function (e) {

        if ($(this).parent('form').valid()) {


            $(this).after("<input type='image'  value='' src='/images/ajax-load1.gif' />");
            $(this).hide();


            var str = getGenerateFormData('form' + $("body").attr("data-id"),
                $("body").attr("data-id")) + '$' + OutputHtmlForm('form' + $("body").attr("data-id")) + '$' +
                $("input[type=text]").eq(1).val() + ' ' + $("input[type=text]").eq(2).val();


            if (($("#txtcaptcha").attr("data-rnd") == Base64.encode($("#txtcaptcha").val())) ||
                $("#txtcaptcha").attr("data-rnd") === undefined) {

                str = postMultiValue(htmlEscape(str), 'RegisterForms', '', "/baseservice.asmx");
                $("form[id^=form" + $("body").attr("data-id") + "]").hide();

                $(".message").show();
                $("#uploader").remove();
                //  $("#simpleerror").remove();

                $('body,html').animate({
                    scrollTop: $(".message").offset().top - 50,
                }, 1600);

            }

        }
        else {

            $('body,html').animate({
                scrollTop: $("input.error").eq(0).offset().top - 100,
            }, 1800);
        }
    });


    $('form[id^=form] input,form[id^=form] textarea').each(function () {

        if ($(this).attr('required') == 'required' && $(this).parents('form').hasClass('isrequired')) {
            $(this).after('*');

        }

    });
    $('body').on("touchstart", '.dropdown ul .parent span', function (e) {

        $(this).parent().find("ul:first").slideToggle(500).show();
    });

    $('.dropdown li').each(function () {


        if ($(this).children('ul').length > 0) {
            $(this).addClass('parent');

            $(this).prepend('<span></span>');

        }

    });

    $('.news-box-picture').each(function () {


        if ($(this).find('img').attr('src') == '') {
            $(this).hide();


        }

    });

    $('input[name=txtSearch],input[name=txtsearch]').keydown(function (e) {

        var key;
        key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

        if (key == 13) {
            e.preventDefault();
           
            $(".icon-search").click();
        }

    });

    $(".icon-search").click(function () {

        var txt = '';

        if ($("input[name=txtSearch]").length)
            txt = $("input[name=txtSearch]").val().replace(/ /g, '-')
        else
            txt = $("input[name=txtsearch]").val().replace(/ /g, '-')

        if (txt != "") {

            document.location.href = "/search/" + txt;
        }
    });
    
    $(window).scroll(function () {
        
        if ($(window).width() > 980) {
            if ($(this).scrollTop() > 650) {
             
                $('.up').fadeIn();
            } else {
               
                $('.up').fadeOut();
            }
        }
    });

    // scroll body to 0px on click
    $('.up').click(function () {
      
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });


    $(".sidebar > ul > li > a").click(function(e){
        e.preventDefault();
    });
      $(".sidebar li").click(function(){
        $(this).find("> ul").toggleClass('gather-menu');
    });
 
});
